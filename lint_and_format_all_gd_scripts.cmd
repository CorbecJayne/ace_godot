@ECHO off
for /r %%i in (*.gd) do (
  if not %%~ni == aseprite_cmd (
  if not %%~ni == config_dialog (
  if not %%~ni == first_options_window (
  if not %%~ni == import_plugin (
  if not %%~ni == plugin (
	gdlint %%i 
	gdformat %%i 
  )))))
)
pause